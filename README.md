# README #

Examples for Game Engine Development module, these are taken from [AICore](https://github.com/idmillington/aicore) and rebuilt using CMake. These are based on the [course text](https://www.amazon.co.uk/Artificial-Intelligence-Games-Ian-Millington/dp/0123747317).  Due to issue with an old version of Glut the original AICore repo produced by Millington for his book can't be compiled in the lab.

## Instructions

As with previous projects:
1. Create a build directory.
2. Generate a build environment using CMake.
3. Build the project using a build framework of your choice (e.g. Visual Studio).
4. Run the demo projects.

## Note

I've not tested building on Windows, from my earlier efforts to build the books sample code I have tried to mitigate issues with **winmm** used for the high precision timing.  I don't know if the fix I've added to the CMakeLists will work. 
